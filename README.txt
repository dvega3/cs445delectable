{\rtf1\ansi\ansicpg1252\cocoartf1404\cocoasubrtf130
{\fonttbl\f0\fswiss\fcharset0 Helvetica;\f1\fnil\fcharset0 Consolas;}
{\colortbl;\red255\green255\blue255;}
\margl1440\margr1440\vieww20080\viewh11920\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\partightenfactor0

\f0\fs24 \cf0 In order to run the project, you will need to run the MAKE file provided under /Code/delectable \
\
To run the project on local host, you can run  
\f1 make all
\f0 \
This will download java, maven, chrome, and run the project\
In order to run again, all you need to run is 
\f1 make
\f0 \
In order to stop the project, just hit the enter key in the terminal\
\
To run the testing script, you can run 
\f1 make test
\f0 \
\
The reason it downloads chrome is so you can better test the project\
with an extension called Postman. The link is available below:\
\
{\field{\*\fldinst{HYPERLINK "https://chrome.google.com/webstore/detail/postman-rest-client-short/mkhojklkhkdaghjjfdnphfphiaiohkef?utm_source=chrome-ntp-icon"}}{\fldrslt https://chrome.google.com/webstore/detail/postman-rest-client-short/mkhojklkhkdaghjjfdnphfphiaiohkef?utm_source=chrome-ntp-icon}}\
\
With this extension, you can add a file that automatically adds all the APIs with the \
correct JSON format for each request. This file is a JSON file itself and is provided in the directory\
The name of this file is delectable.json. In order to add this, once postman is downloaded, open postman, go to\
Collections, and there is a button with an inbox icon to import a collection. Press the button and chose the json file provided.\
Once this is done, you will be able to post, put, and get with the API described in the project.\
\
If you rather do it manually through another tool, I have provided a file with the accepted json format for each request.\
It will have the API followed by the format of the request.\
\
If you have any questions, please contact me at dvega3@hawk.iit.edu\
\
\
\
\
\
}