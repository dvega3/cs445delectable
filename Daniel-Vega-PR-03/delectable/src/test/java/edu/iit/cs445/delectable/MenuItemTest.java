package edu.iit.cs445.delectable;
import static org.junit.Assert.*;
import org.junit.Test;

public class MenuItemTest {

	@Test
	public void notNullConstructorTest() {
		MenuItem m = new MenuItem();
		assertNotNull(m);
	}
	
	@Test
	public void notNullNDConstructorTest() {
		MenuItem m = new MenuItem("Pizza",5.55,10,new String[]{"Vegan"});
		assertNotNull(m);
	}
	
	@Test
	public void gettersAndSettersTest() {
		MenuItem m = new MenuItem();
		String name = "Pizza";
		double pricePP = 10.0;
		int minOrder = 5;
		String []cat = new String[5];
		cat[0]="Vegetarian";
		m.setName(name);
		m.setPricePP(pricePP);
		m.setMinOrder(minOrder);
		m.setCat(cat);
		assertEquals(m.getName(),name);
		assertEquals(m.getPricePP(), pricePP,0f);
		assertEquals(m.getMinOrder(),minOrder);
		assertEquals(m.getCat(),cat);
	}

}
