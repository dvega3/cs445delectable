package edu.iit.cs445.delectable;

import static org.junit.Assert.*;
import org.junit.Test;

public class CustomerInfoTest {

	@Test
	public void NotNullConstructor() {
		CustomerInfo c = new CustomerInfo();
		assertNotNull(c);
	}
	
	@Test
	public void NotNullNDConstructorTest() {
		CustomerInfo c = new CustomerInfo("Daniel","dan@gmail.com","7733075945");
		assertNotNull(c);
	}
	
	@Test
	public void gettersSettersTest() {
		CustomerInfo c = new CustomerInfo();
		String e = "dvega3@hawk.iit.edu";
		String m = "7733075945";
		String n = "Daniel Vega";
		int i = 0;
		c.setEmail(e);
		c.setId(i);
		c.setMobile(m);
		c.setName(n);
		assertEquals(c.getEmail(),e);
		assertEquals(c.getId(),i);
		assertEquals(c.getMobile(),m);
		assertEquals(c.getName(),n);
	}
	
	@Test
	public void findTest() {
		CustomerInfo c = new CustomerInfo();
		assertEquals(true,c.find("John"));
		assertEquals(false,c.find("Sample"));
	}

}
