package edu.iit.cs445.delectable;

import static org.junit.Assert.*;
import org.junit.Test;

public class ReportRevenueTest {

	@Test
	public void notNullConstructor() {
		ReportRevenue r = new ReportRevenue();
		assertNotNull(r);
	}
	
	@Test 
	public void notNullNDConstructor() {
		ReportRevenue r = new ReportRevenue(2);
		assertNotNull(r);
	}
	
	@Test
	public void gettersAndSettersTest() {
		ReportRevenue r = new ReportRevenue();
		r.setStart_date("20160423");
		r.setEnd_date("20160430");
		r.setOrders_placed(0);
		r.setOrders_cancelled(0);
		r.setOrders_open(0);
		r.setFood_revenue(0);
		r.setSurcharge_revenue(0);
		r.doReport(null,null);
		r.doReport("20160423","20160430");
		assertEquals("20160423", r.getStart_date());
		assertEquals("20160430", r.getEnd_date());
		assertEquals(1,r.getOrders_placed());
		assertEquals(0,r.getOrders_cancelled());
		assertEquals(1,r.getOrders_open());
		assertEquals(0,r.getFood_revenue(),0f);
		assertEquals(0,r.getSurcharge_revenue(),0f);
		
	}

}
