package edu.iit.cs445.delectable;

import static org.junit.Assert.*;
import org.junit.Test;

public class OrderDetailTest {

	@Test
	public void notNullConstructor() {
		OrderDetail od = new OrderDetail();
		assertNotNull(od);
	}
	
	@Test
	public void gettersAndSetters() {
		OrderDetail od = new OrderDetail();
		MenuItem m = new MenuItem();
		Storage.addMenuItem(m);
		String id="0";
		String name = "Pizza";
		int count = 5;
		od.setName(name);
		od.setCount(count);
		od.setId(id);
		assertEquals(od.getCount(),count);
		assertEquals(od.getName(),name);
		assertEquals(od.getId(),id);
	}

}
