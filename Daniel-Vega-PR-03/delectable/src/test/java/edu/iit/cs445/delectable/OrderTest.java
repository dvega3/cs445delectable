package edu.iit.cs445.delectable;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;

public class OrderTest {

	@Test
	public void notNullConstructorTest() {
		Order o = new Order();
		assertNotNull(o);
	}
	
	@Test
	public void gettersAndSettersTest() {
		Order o = new Order();
		MenuItem m = new MenuItem();
		Storage.addMenuItem(m);
		ArrayList<OrderDetail>orderDetail = new ArrayList<OrderDetail>();
		String deliveryDate = "20160525";
		String deliveryAddress = "1532 N Monticello Ave, Chicago IL 60651";
		String note = "fist floor";
		String status= "open";
		double surcharge = 0;
		o.setOrderDetail(orderDetail);
		o.setDeliveryDate(deliveryDate);
		o.setDeliveryAddress(deliveryAddress);
		o.setNote(note);
		o.setStatus(status);
		o.setSurcharge(surcharge);
		o.setOrderDate("20160423");
		o.calculateAmount();
		o.setAmount(0);
		assertEquals(o.getOrderDetail(),orderDetail);
		assertEquals(o.getDeliveryAddress(),deliveryAddress);
		assertEquals(o.getDeliveryDate(),deliveryDate);
		assertEquals(o.getNote(),note);
		assertEquals(o.getStatus(),status);
		assertEquals(o.getSurcharge(),surcharge,0f);
		assertEquals("20160423",o.getOrderDate());
		assertEquals(0,o.getAmount(),0f);
		assertEquals(true,o.isDeliveredOn(deliveryDate));
	}
}
