package edu.iit.cs445.delectable;

import static org.junit.Assert.*;
import org.junit.Test;

public class StorageTest {

	@Test
	public void surchargeTest() {
		Storage.setSurcharge(5.00);
		assertEquals(5.00,Storage.getSurcharge(),0f);
	}
	
	@Test
	public void getMenuItemTest() {
		MenuItem m = new MenuItem();
		Storage.addMenuItem(m);
		assertEquals(m,Storage.getMenuItem(Integer.toString(m.getId())));
	}
	
	@Test 
	public void getCustomerTest() {
		CustomerInfo c = new CustomerInfo();
		Storage.addCustomer(c);
		assertEquals(c,Storage.getCustomer(Integer.toString(c.getId())));
	}
	
	@Test 
	public void findCustomerTest() {
		CustomerInfo c = new CustomerInfo();
		Storage.addCustomer(c);
		assertEquals(1,Storage.findCustomer("Doe").size());
	}
	
	@Test
	public void getOrderTest() {
		Order o = new Order();
		Storage.addOrder(o);
		Storage.cancelOrder(Integer.toString(o.getId()));
		assertEquals(o,Storage.getOrder(Integer.toString(o.getId())));
	}
	
	@Test
	public void getOrdersDeliveredTest() {
		Order o = new Order();
		Storage.addOrder(o);
		Storage.getReports();
		assertEquals(0,Storage.getOrdersDeliveredOn("20160423").size());
	}

}
