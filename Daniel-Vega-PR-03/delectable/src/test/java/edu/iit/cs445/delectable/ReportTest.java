package edu.iit.cs445.delectable;

import static org.junit.Assert.*;
import org.junit.Test;

public class ReportTest {

	@Test
	public void notNullConstructor() {
		Report r = new Report(1);
		assertNotNull(r);
	}
	
	@Test
	public void gettersAndSettersTest() {
		Report r = new Report(0);
		r.setId(0);
		assertEquals(r.getId(),0);
		assertEquals(r.getName(),"Orders to deliver today");
	}

}