package edu.iit.cs445.delectable;

import static org.junit.Assert.*;
import org.junit.Test;

public class ReportDeliveryTest {
	
	@Test
	public void gettersAndSettersTest() {
		ReportDelivery r = new ReportDelivery(0);
		r.setOrders_delivered(10);
		r.setStart_date("20160423");
		r.setEnd_date("20160430");
		assertEquals(r.getOrders_delivered(),10);
		assertEquals(r.getStart_date(),"20160423");
		assertEquals(r.getEnd_date(),"20160430");
	}
	
	@Test
	public void notNullNDConstructor() {
		ReportDelivery r = new ReportDelivery();
		assertNotNull(r);
	}
	
	@Test
	public void notNullConstrucotor() {
		ReportDelivery r = new ReportDelivery(0);
		assertNotNull(r);
	}
	
	@Test
	public void doReportTest() {
		ReportDelivery r = new ReportDelivery(3);
		Order o = new Order();
		o.setStatus("delivered");
		Storage.addOrder(o);
		r.doReport("20160423","20160430");
		r.doReport(null,null);
		assertEquals(1,r.orders_placed);
		assertEquals(0,r.orders_delivered);
		
	}
	
	
}
