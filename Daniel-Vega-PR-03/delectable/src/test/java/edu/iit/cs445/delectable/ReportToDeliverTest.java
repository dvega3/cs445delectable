package edu.iit.cs445.delectable;

import static org.junit.Assert.*;
import org.junit.Test;

public class ReportToDeliverTest {

	@Test
	public void notNullConstructorTest() {
		ReportToDeliver r = new ReportToDeliver();
		assertNotNull(r);
	}
	
	@Test
	public void doReportTest(){
		Storage.addOrder(new Order());
		ReportToDeliver r = new ReportToDeliver(0);
		r.doReport();
		assertEquals(0,r.getOrders().size());
	}
	
	@Test
	public void doOtherReportTest() {
		Storage.addOrder(new Order());
		ReportToDeliver r = new ReportToDeliver(1);
		r.doReport();
		assertEquals(0,r.getOrders().size());
	}

}
