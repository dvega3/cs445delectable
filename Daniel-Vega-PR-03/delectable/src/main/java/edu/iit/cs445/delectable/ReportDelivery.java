package edu.iit.cs445.delectable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ReportDelivery extends Report{
	
	int orders_delivered, orders_placed;
	String start_date, end_date;
	ArrayList<Order> orders;
	
	public ReportDelivery(int i) {
		super(i);
		orders_delivered = 0;
		orders = new ArrayList<Order>();
	}
	
	public ReportDelivery() {
		super();
		orders = new ArrayList<Order>();
	}
	public ArrayList<Order> getOrders() {
		return orders;
	}

	public void setOrders(ArrayList<Order>orders) {
		this.orders = orders;
	}
	
	public int getOrders_delivered() {
		return orders_delivered;
	}
	public void setOrders_delivered(int orders_delivered) {
		this.orders_delivered = orders_delivered;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public void doReport(String s, String e) {
		orders.clear();
		start_date = s;
		end_date   = e;
		orders_placed = 0;
		orders_delivered = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		try {
			
			if(s!=null && e!=null) {
				Date sD = sdf.parse(start_date);
				Date eD = sdf.parse(end_date);
				for(Order o:Storage.getOrdersDeliveryBetween(sD, eD)) {
					orders_placed++;
					if(o.getStatus().equals("open")) {
						orders_delivered++;
						orders.add(o);
					}
				}
			} else {
				for(Order o:Storage.getOrders()) {
					orders_placed++;
					if(o.getStatus().equals("open")) {
						orders.add(o);
						orders_delivered++;
					}
				}
			}
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
