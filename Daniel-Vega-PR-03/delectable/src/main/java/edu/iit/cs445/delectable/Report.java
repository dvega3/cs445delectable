package edu.iit.cs445.delectable;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.core.Context;

public class Report {
	
	int id;
	String name;
	
	public Report(int i) {
		id = i;
		reportType(i);
	}
	
	public Report() {
		id = -1;
		name = "Not a valud report id";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void reportType(int i) {
		switch(i) {
			case 0: name = "Orders to deliver today";
			break;
			case 1: name = "Orders to deliver tomorrow";
			break;
			case 2: name = "Revenue report";
			break;
			case 3: name = "Orders delivery report";
			break;
			default: name = "Not a valud report id";
			break;
		}
	}
	
	public void doReport() {
		
	}
}
