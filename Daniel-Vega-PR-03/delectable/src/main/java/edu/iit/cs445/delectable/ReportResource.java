package edu.iit.cs445.delectable;

import java.util.Collection;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/report")
public class ReportResource {
	
	//@Context ReportDao d;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Report> getReport() {
		return Storage.getReports();
	}
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response something(@PathParam("id") String id,
							  @QueryParam("start_date") String start_date,
							  @QueryParam("end_date") String end_date) {
		Storage.addReportTypes();
		int i = Integer.parseInt(id);
		if(i>=0 && i<=1 && start_date==null && end_date==null) {
			ReportToDeliver r =  (ReportToDeliver) Storage.reports.get(id);
			r.doReport();
			return Response.status(201).entity(r).build();
		} else if(i==2) {
			ReportRevenue rr = (ReportRevenue) Storage.reports.get(id);
			rr.doReport(start_date, end_date);
			return Response.status(201).entity(rr).build();
		}else if(i==3) {
			ReportDelivery rd = (ReportDelivery) Storage.reports.get(id);
			rd.doReport(start_date, end_date);
			return Response.status(201).entity(rd).build();
		}
		return Response.status(404).build();
	}
	
}
