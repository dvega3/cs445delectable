package edu.iit.cs445.delectable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReportRevenue extends Report {
	
	String start_date,end_date;
	int orders_placed, orders_cancelled, orders_open;
	double food_revenue, surcharge_revenue;
	
	public ReportRevenue(int i) {
		super(i);
		orders_placed     = 0;
		orders_cancelled  = 0;
		orders_open       = 0;
		food_revenue      = 0;
		surcharge_revenue = 0;
	}

	public ReportRevenue() {
		super();
		orders_placed = 0;
		orders_cancelled= 0;
		orders_open=0;
		food_revenue = 0;
		surcharge_revenue=0;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public int getOrders_placed() {
		return orders_placed;
	}

	public void setOrders_placed(int orders_placed) {
		this.orders_placed = orders_placed;
	}

	public int getOrders_cancelled() {
		return orders_cancelled;
	}

	public void setOrders_cancelled(int orders_cancelled) {
		this.orders_cancelled = orders_cancelled;
	}

	public int getOrders_open() {
		return orders_open;
	}

	public void setOrders_open(int orders_open) {
		this.orders_open = orders_open;
	}

	public double getFood_revenue() {
		return food_revenue;
	}

	public void setFood_revenue(double food_revenue) {
		this.food_revenue = food_revenue;
	}

	public double getSurcharge_revenue() {
		return surcharge_revenue;
	}

	public void setSurcharge_revenue(double surcharge_revenue) {
		this.surcharge_revenue = surcharge_revenue;
	}
	
	public void doReport(String s, String e) {
		start_date = s;
		end_date   = e;
		orders_placed     = 0;
		orders_cancelled  = 0;
		orders_open       = 0;
		food_revenue      = 0;
		surcharge_revenue = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		try {
			
			if(s!=null && e!=null) {
				Date sD = sdf.parse(start_date);
				Date eD = sdf.parse(end_date);
				for(Order o:Storage.getOrdersBetween(sD, eD)) {
					orders_placed++;
					if(o.getStatus().equals("open")|| o.getStatus().equals("delivered")) {
						orders_open++;
						food_revenue+=o.getAmount();
						surcharge_revenue+= o.getSurcharge();
					} else if(o.getStatus().equals("cancelled")) {
						orders_cancelled++;
					}
				}
			} else {
				for(Order o:Storage.getOrders()) {
					orders_placed++;
					if(o.getStatus().equals("open")) {
						orders_open++;
						food_revenue+=o.getAmount();
						surcharge_revenue+= o.getSurcharge();
					} else if (o.getStatus().equals("delivered")) {
						food_revenue+=o.getAmount();
						surcharge_revenue+= o.getSurcharge();
					} else if(o.getStatus().equals("cancelled")) {
						orders_cancelled++;
					}
				}
			}
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
