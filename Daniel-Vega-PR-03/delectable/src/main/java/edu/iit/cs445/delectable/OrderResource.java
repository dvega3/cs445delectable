package edu.iit.cs445.delectable;

import java.util.Collection;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/order")
public class OrderResource {
	
	//@Context OrderDao d;
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Order> getOrders(@QueryParam("date")String date) {
		if(date == null)
			return Storage.getOrders();
		return Storage.getOrdersDeliveredOn(date);
	}
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOrder(@PathParam("id") String id) {
		Order or = Storage.getOrder(id);
		if(or==null)
			return Response.status(404).build();
		else
			return Response.status(200).entity(or).build();
	}
	
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addMenuItem(Order m) {
		String s = Storage.addOrder(m);
		if(s.equals("Invalid Order")) {
			return Response.status(400).entity(s).build();
		}
		return Response.status(201).entity(s).build();
	}
	
	@Path("/cancel/{id}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void cancelOrder(@PathParam("id") String id) {
		Storage.cancelOrder(id);
	}

}