package edu.iit.cs445.delectable;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

public class Application extends ResourceConfig {
	Application(){
		//final MenuItemDao dM     = new MenuItemDao();
		//final OrderDao dO        = new OrderDao();
		//final CustomerInfoDao dC = new CustomerInfoDao();
		//final ReportDao dR       = new ReportDao();
		
		packages("edu.iit.cs445.delectable");
		register(new AbstractBinder(){
			protected void configure(){
				//bind(dM).to(MenuItemDao.class);
				//bind(dO).to(OrderDao.class);
				//bind(dC).to(CustomerInfoDao.class);
			  //bind(dR).to(ReportDao.class);
			}
		});
	}
}
