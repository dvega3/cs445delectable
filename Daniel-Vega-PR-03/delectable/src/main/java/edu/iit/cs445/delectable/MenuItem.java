package edu.iit.cs445.delectable;

public class MenuItem {

	String name;
	double pricePP;
	int minOrder;
	String []cat;
	private int id;
	
	public MenuItem() {
		name = "Pizza";
		pricePP = 10.0;
		minOrder = 5;
		cat = new String[5];
		cat[0]="Vegetarian";
		id = IdGenerator.newIdMenu();
	}
	
	public MenuItem(String n, double p, int m, String []c) {
		name = n;
		pricePP = p;
		minOrder = m;
		cat = c;
		id = IdGenerator.newIdMenu();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPricePP() {
		return pricePP;
	}

	public void setPricePP(double pricePP) {
		this.pricePP = pricePP;
	}

	public int getMinOrder() {
		return minOrder;
	}

	public void setMinOrder(int minOrder) {
		this.minOrder = minOrder;
	}

	public String[] getCat() {
		return cat;
	}

	public void setCat(String[] cat) {
		this.cat = cat;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int i) {
		id = i;
	}
}
