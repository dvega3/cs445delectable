package edu.iit.cs445.delectable;

public class CustomerInfo {
	String name, email, mobile;
	private int id;
	
	public CustomerInfo() {
		name = "John Doe";
		email = "jdoe@gmail.com";
		mobile = "123-456-7890";
		id = IdGenerator.newCId();
	}
	
	public CustomerInfo(String n, String e, String m) {
		name = n;
		email = e;
		mobile = m;
		id = IdGenerator.newCId();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public boolean find(String key) {
		key = key.toLowerCase();
		if(name.toLowerCase().contains(key))
			return true;
		else if (email.toLowerCase().contains(key))
			return true;
		else if (mobile.toLowerCase().contains(key))
			return true;
		else
			return false;
	}
}