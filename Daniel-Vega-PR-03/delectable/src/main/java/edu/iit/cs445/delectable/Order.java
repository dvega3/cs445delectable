package edu.iit.cs445.delectable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.core.Context;

public class Order {
	private SimpleDateFormat date;
	final private String dateFormat = "yyyyMMdd";
	String orderDate,deliveryDate;
	CustomerInfo orderedBy;
	ArrayList<OrderDetail> orderDetail;
	String status, note,deliveryAddress;
	double amount, surcharge;
	boolean totalCalculated;
	private int id;
	
	public Order() {
		orderDetail = new ArrayList<OrderDetail>();
		Date m = new Date();
		date = new SimpleDateFormat(dateFormat);
		orderDate = date.format(m);
		deliveryDate = "20160525";
		deliveryAddress = "1532 N Monticello Ave, Chicago IL 60651";
		note = "fist floor";
		status= "open";
		surcharge = 0;
		totalCalculated = false;
		id=IdGenerator.newIdOrder();
	}
	
	public double getSurcharge() {
		return surcharge;
	}

	public void setSurcharge(double surcharge) {
		this.surcharge = surcharge;
	}
	public ArrayList<OrderDetail> getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(ArrayList<OrderDetail> orderDetail) {
		this.orderDetail = orderDetail;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public CustomerInfo getOrderedBy() {
		return orderedBy;
	}

	public void setOrderedBy(CustomerInfo personalInfo) {
		this.orderedBy = personalInfo;
		Storage.addCustomer(personalInfo);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public double getAmount() {
		if(totalCalculated == false) {
			amount = calculateAmount();
			totalCalculated = true;
		}
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public double calculateAmount() {
		double total=0;
		
		for(OrderDetail o:orderDetail)
			total += o.getAmount();//d.getMenuItem(Integer.toString(o.getId())).getPricePP() * o.getCount();
		
		try {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date dd;
			dd = sdf.parse(deliveryDate);
			cal.setTime(dd);
			boolean weekend = (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) ||
							  (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY);
			if(weekend) {
				surcharge = Storage.surcharge;
			}
			else
				surcharge = 0;
		} catch (ParseException e) {
			surcharge = 0;
			e.printStackTrace();
		}
		total+=surcharge;
		return total;
	}
	
	public boolean isDeliveredOn(String date) {
		if(deliveryDate.equals(date))
			return true;
		else
			return false;
	}
}