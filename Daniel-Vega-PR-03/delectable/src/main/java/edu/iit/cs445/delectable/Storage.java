package edu.iit.cs445.delectable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Storage {
	
	
	static double surcharge = 0;
	static boolean reportsInit = false;
	static Map<String,MenuItem> food = new ConcurrentHashMap<String,MenuItem>();
	static Map<String,CustomerInfo> customers = new ConcurrentHashMap<String,CustomerInfo>();
	static Map<String,Order> orders = new ConcurrentHashMap<String,Order>();
	static Map<String,Report> reports = new ConcurrentHashMap<String,Report>();

	public static double getSurcharge() {
		return surcharge;
	}

	public static void setSurcharge(double surcharge) {
		Storage.surcharge = surcharge;
	}
	
	public static String addMenuItem(MenuItem m) {
		food.put(Integer.toString(m.getId()) ,m);
		return "{\n\"id\": " + m.getId() + "\n}";
	}
	
	public static MenuItem getMenuItem(String ID) {
		return (food.get(ID));
	}
	
	public static Collection <MenuItem> getMenuItems() {
		return (food.values());
	}
	
	public static Collection <CustomerInfo> getCustomers() {
		return (customers.values());
	}
	
	public static CustomerInfo getCustomer(String id) {
		for(CustomerInfo c:getCustomers()) {
			if(id.equals(Integer.toString(c.getId())))
				return c;
		}
		return null;
	}
	
	public static void addCustomer(CustomerInfo o) {
		CustomerInfo c = customers.get(o.getEmail());
		if(c ==null) {
			customers.put(o.getEmail() ,o);
		} else {
			o.setId(c.getId());
		}
	}
	
	public static Collection<CustomerInfo> findCustomer(String key) {
		Map<String,CustomerInfo> cc = new ConcurrentHashMap<String,CustomerInfo>();
		for(CustomerInfo c:getCustomers()) {
			if(c.find(key))
				cc.put(Integer.toString(c.getId()), c);
		}
		return cc.values();
	}
	
	public static Collection <Order> getOrders() {
		return (orders.values());
	}
	
	public static Order getOrder(String ID) {
		return (orders.get(ID));
	}

	public static String addOrder(Order o) {
		
		boolean validOrder = true;
		for(OrderDetail od: o.getOrderDetail()) {
			if(od.isValidCount()==false) {
				validOrder = false;
				break;
			}
		}
		if(validOrder) {
			orders.put(Integer.toString(o.getId()) ,o);
			return "{\n\"id\": "+o.getId()+",\n"
			+ "\"cancel_url\": \"/order/cancel/"+o.getId()+"\"\n}";
		} else {
			return "Invalid Order";
		}
	}
	
	public static void cancelOrder(String ID) {
		SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");
		Date m = new Date();
		String dd = date.format(m);
		if(!dd.equals(orders.get(ID).getDeliveryDate()))
			orders.get(ID).setStatus("cancelled");
	}
	
	public static Collection<Order> getOrdersDeliveredOn(String date) {
		Map<String,Order> oo = new ConcurrentHashMap<String,Order>();
		for(Order o:getOrders()) {
			if(o.isDeliveredOn(date))
				oo.put(Integer.toString(o.getId()), o);
		}
		return oo.values();
	}
	
	public static Collection<Report> getReports() {
		if(reportsInit==false) {
			addReportTypes();
			reportsInit = true;
		}
		return reports.values();
	}
	
	public static void setReports(Map<String, Report> reports) {
		Storage.reports = reports;
	}

	public static void addReportTypes() {
		if(reportsInit==false) {
			reports.put("0", new ReportToDeliver(0));
			reports.put("1", new ReportToDeliver(1));
			reports.put("2", new ReportRevenue(2));
			reports.put("3", new ReportDelivery(3));
			reportsInit = true;
		}
	}
	
	public static Collection<Order> getOrdersBetween(Date s, Date e) {
		Map<String,Order> oo = new ConcurrentHashMap<String,Order>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date oD;
		for(Order o:getOrders()) {
			try {
				oD = sdf.parse(o.getOrderDate());
				if((oD.equals(e) || oD.before(e)) && (oD.after(s) || oD.equals(s)))
					oo.put(Integer.toString(o.getId()), o);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return oo.values();
	}
	public static Collection<Order> getOrdersDeliveryBetween(Date s, Date e) {
		Map<String,Order> oo = new ConcurrentHashMap<String,Order>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date oD;
		for(Order o:getOrders()) {
			try {
				oD = sdf.parse(o.getDeliveryDate());
				if((oD.equals(e) || oD.before(e)) && (oD.after(s) || oD.equals(s)))
					oo.put(Integer.toString(o.getId()), o);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return oo.values();
	}
 }
