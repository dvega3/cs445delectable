package edu.iit.cs445.delectable;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/customer")
public class CustomerInfoResource {

	//@Context CustomerInfoDao d;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<CustomerInfo> getCustomers(@QueryParam("key")String key) {
		if(key == null)
			return Storage.getCustomers();
		return Storage.findCustomer(key);
	}
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomer(@PathParam("id") String id) {
		CustomerInfo ci = Storage.getCustomer(id);
		if(ci==null)
			return Response.status(404).build();
		else
			return Response.status(200).entity(ci).build();
	}
	
	

}
