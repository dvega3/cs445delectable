package edu.iit.cs445.delectable;
import java.util.Collection;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/menu")
public class MenuItemResource {

	//@Context MenuItemDao d;
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<MenuItem> getMenuItems() {
		return Storage.getMenuItems();
	}
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMenuItem(@PathParam("id") String id) {
		MenuItem m = Storage.getMenuItem(id);
		if(m==null)
			return Response.status(404).build();
		else
			return Response.status(200).entity(m).build();
	}
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addMenuItem(MenuItem m) {
		return Storage.addMenuItem(m);
	}
}
