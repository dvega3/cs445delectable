package edu.iit.cs445.delectable;

import javax.ws.rs.core.Context;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

public class OrderDetail {
	
	String name;
	String id;
	int count;
	
	public OrderDetail() {
		id="0";
		name = "Pizza";
		count = 5;
	}
	
	public double getAmount() {
		MenuItem m = Storage.getMenuItem(id);
		return m.getPricePP()*count;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	public boolean isValidCount() {
		MenuItem m = Storage.getMenuItem(id);
		if(count>=m.getMinOrder())
			return true;
		else
			return false;
	}
	
}
