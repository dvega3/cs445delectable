package edu.iit.cs445.delectable;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/admin")
public class AdmnResourse {

	//@Context MenuItemDao d;
	//@Context OrderDao dd;
	
	@Path("/menu")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addMenuItem(MenuItem m) {
		return Response.status(201).entity(Storage.addMenuItem(m)).build();
	}
	
	@Path("/menu/{id}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response modifyMenuItem(@PathParam("id") String id,final String json) {
		MenuItem m = Storage.getMenuItem(id);
		if(m==null)
			return Response.status(400).build();
		Pattern pp =Pattern.compile("\\d+(\\.\\d{1,2})");
		Matcher mss = pp.matcher(json);
		if(mss.find()) {
			m.setPricePP(Double.parseDouble(mss.group()));//.replaceAll("\\D+",""));
			return Response.status(204).build();
		}
		
		return Response.status(400).build();
	}
	
	@Path("/surcharge")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getSurcharge() {
		return "{\n"
				+ "\"surcharge\":"+Storage.getSurcharge()
				+"\n}";
	}
	
	@Path("/surcharge")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSurcharge(final String json) {
		Pattern pp =Pattern.compile("\\d+(\\.\\d{0,2})");
		Matcher mss = pp.matcher(json);
		if(mss.find()) {
			Storage.setSurcharge(Double.parseDouble(mss.group()));//.replaceAll("\\D+",""));
			return Response.status(204).build();
		}
		return Response.status(400).build();
	}
	
	@Path("/delivery/{id}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deliverOrder(@PathParam("id") String id) {
		Order r = Storage.getOrder(id);
		if(r==null)
			return Response.status(404).build();
		else {
			r.setStatus("delivered");
			return Response.status(204).build();
		}
	}
}
