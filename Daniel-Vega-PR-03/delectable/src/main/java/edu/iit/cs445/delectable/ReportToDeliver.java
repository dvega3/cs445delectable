package edu.iit.cs445.delectable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ReportToDeliver extends Report {

	ArrayList<Order> orders;
	
	public ReportToDeliver(int i) {
		super(i);
		orders = new ArrayList<Order>();
	}

	public ReportToDeliver() {
		super();
		orders = new ArrayList<Order>();
	}
	
	public ArrayList<Order> getOrders() {
		return orders;
	}

	public void setOrders(ArrayList<Order>orders) {
		this.orders = orders;
	}
	
	public void doReport() {
		orders.clear();
		Date date = new Date();
		String day;
		if(super.getId()==0) {
			day = new SimpleDateFormat("yyyyMMdd").format(date);
			for(Order o:Storage.getOrdersDeliveredOn(day)) {
				if(o.getStatus().equals("open"))
					orders.add(o);
			}
		} else if(super.getId()==1) {
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.DATE, 1);
			date = c.getTime();
			day = new SimpleDateFormat("yyyyMMdd").format(date);
			for(Order o:Storage.getOrdersDeliveredOn(day)) {
				if(o.getStatus().equals("open"))
					orders.add(o);
			}
		}
	}

	

}
