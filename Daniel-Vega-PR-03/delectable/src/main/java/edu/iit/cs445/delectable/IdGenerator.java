package edu.iit.cs445.delectable;
import java.util.concurrent.atomic.AtomicInteger;

public class IdGenerator {
	private static AtomicInteger menId = new AtomicInteger();
	private static AtomicInteger orderId = new AtomicInteger();
	private static AtomicInteger cId = new AtomicInteger();
	
	public static int newIdMenu() {
		return menId.getAndIncrement();
	}

	public static int newIdOrder() {
		return orderId.getAndIncrement();
	}
	
	public static int newCId() {
		return cId.getAndIncrement();
	}
}

